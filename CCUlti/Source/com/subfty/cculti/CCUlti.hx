package com.subfty.cculti;

import flash.events.MouseEvent;
import flash.text.TextFormatAlign;
import flash.Lib;
import flash.text.TextFormat;
import flash.text.TextField;
import com.subfty.cculti.actors.CookieFactory;
import flash.display.Bitmap;
import openfl.Assets;
import haxe.xml.Fast;
import flash.display.StageAlign;
import de.polygonal.core.math.random.Random;
import com.subfty.cculti.actors.Cookie;
import flash.events.Event;
import flash.display.Sprite;



class CCUlti extends Sprite {

    public static var config : Fast;
    public static var stageW : Float;
    public static var stageH : Float;

    public static var gameFinished : Bool;

    var cookieFact : CookieFactory;
    var cookies    : Array<Cookie>;

    var timer : TextField;
	var startTime : Int;
    var endTime : Int;

    var confirmNewGame : Int = 0;

	public function new () {
		super ();

        addEventListener(Event.ADDED_TO_STAGE, init);
        addEventListener(Event.ENTER_FRAME, onEnterFrame);
	}

    function init(e : Event){
        trace("Cookie Clicker Ultimate!");

        stageW = stage.stageWidth;
        stageH = stage.stageHeight;

      //loading xml
        var xmlData : String = Assets.getText("assets/config.xml");
        config = new Fast(haxe.xml.Parser.parse(xmlData).firstChild());

      //initing background
        var background : Bitmap = new Bitmap(Assets.getBitmapData("images/background.jpg"));
        background.x = 0;
        background.y = 0;
        background.width = stageW;
        background.height = stageH;
        this.addChild(background);

      //creating clock
        var textFormat : TextFormat = new TextFormat("_sans", 60.0, 0xffffff, true);
        textFormat.align = TextFormatAlign.CENTER;
		//textFormat.align = TextFormatAlign.;

        timer = new TextField();
        timer.x = 0;
        timer.y = 0;
        timer.width = stageW;
        timer.height = 300;
        timer.selectable = false;
        timer.defaultTextFormat = textFormat;

        timer.addEventListener(MouseEvent.CLICK, function(e : Event){
            if(gameFinished && confirmNewGame++ == 2){
                confirmNewGame = 0;
                startNewGame();
            }
        });

        this.addChild(timer);

      //initing cookies
        cookieFact = new CookieFactory();
        cookies = [];

        for(i in 0 ... Std.parseInt(config.node.cookies.att.chockolateChips))
            cookies.push(cookieFact.produceCookie( ChockolateChip ));

        for(i in 0 ... Std.parseInt(config.node.cookies.att.doughnuts))
            cookies.push(cookieFact.produceCookie( Doughnut ));

        var tmp : Array<Cookie> = [];
        for(c in cookies)
            tmp.push(c);

        while(tmp.length > 0){
            var pos : Int = Random.randRange(0, tmp.length - 1);
            this.addChild(tmp[pos]);
            tmp.remove(tmp[pos]);
        }

        startNewGame();
    }

    function startNewGame(){
        startTime = Lib.getTimer();
        endTime = -1;
        gameFinished = false;

        for(c in cookies)
            c.startCookie();
    }

    function onEnterFrame(e : Event){

        if(gameFinished){
            if(endTime == -1)
                endTime = Lib.getTimer();
            var delay = endTime - startTime;
            timer.htmlText = "FIN!\n" +
                             (Std.int(delay / 1000 / 60) % 60) + ":" +
                              Std.int((delay / 1000) % 60) + ":" +
                              Std.int((delay / 10) % 100);
        }else{
            var delay = Lib.getTimer() - startTime;
            timer.htmlText = (Std.int(delay / 1000 / 60) % 60) + ":" +
                              Std.int((delay / 1000) % 60) + ":" +
                              Std.int((delay / 10) % 100);

            gameFinished = true;
            for(c in cookies)
                if(c.visible){
                    gameFinished = false;
                    break;
                }
        }

    }
}