package com.subfty.cculti.actors;

import com.subfty.cculti.CCUlti;
import flash.display.BitmapData;
import openfl.Assets;

using haxe.EnumTools;

enum CookieType {
    ChockolateChip;
    Doughnut;
}

class CookieFactory {
    static var bitmapPaths : Array<Array<String>> = [["images/Cookie.png",
                                                      "images/Cookie1.png",
                                                      "images/Cookie2.png",
                                                      "images/Cookie3.png"],
                                                     ["images/Doughnut.png",
                                                      "images/Doughnut1.png",
                                                      "images/Doughnut2.png",
                                                      "images/Doughnut3.png"]];
    static var bitmapDatas : Array<Array<BitmapData>>;

    public function new() {
        bitmapDatas = [for(arr in bitmapPaths) [for(src in arr) Assets.getBitmapData(src)]];
    }

    public function produceCookie(type : CookieType) : Cookie{
        var newCookie = new Cookie(bitmapDatas[type.getIndex()],
                                   Std.parseFloat(CCUlti.config.node.cookies.att.radius));
        return newCookie;
    }
}
