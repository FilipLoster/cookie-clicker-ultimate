package com.subfty.cculti.actors;

import flash.display.BitmapData;
import de.polygonal.core.math.random.Random;
import motion.easing.Bounce;
import motion.easing.Sine;
import motion.Actuate;
import flash.media.Sound;
import flash.events.TouchEvent;
import de.polygonal.core.math.Mathematics;
import flash.events.MouseEvent;
import flash.events.Event;
import flash.display.Bitmap;
import flash.display.Sprite;

class Cookie extends Sprite{

    public var radius : Float;
    var rotationSpeed : Float;
    var clickStatus : Int;

    var bmpDatas     : Array<BitmapData>;
    var bitmap : Bitmap;

    public function new(bmpDatas : Array<BitmapData>,
                        ?radius : Float = 30) {
        super();

        this.bmpDatas = bmpDatas;

        this.radius = radius;

        this.addEventListener(MouseEvent.CLICK, onClicked);
        this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
    }

    public function startCookie(){
        this.visible = true;

        clickStatus = 0;
        x = Random.frandRange(0, CCUlti.stageW) - radius;
        y = Random.frandRange(0, CCUlti.stageH) - radius;
        rotationSpeed = Random.frandRange(-1.0, 1.0);

        scaleX = scaleY = 1.0;

        Actuate.stop(this, {x : null, y : null, scaleX : null, scaleY : null}, false, false);
        reloadCookie();
        tweenToNewPosition();
    }

    function tweenToNewPosition(){
        Actuate.tween(this, Random.frandRange(4.0, 8.0),
                      {x : Random.frandRange(0, CCUlti.stageW),
                       y : Random.frandRange(0, CCUlti.stageH)})
               .ease( if (Random.randRange(2, 10) == 1) Bounce.easeOut else Sine.easeInOut )
               .onComplete(tweenToNewPosition);
    }

    function reloadCookie(){
        if(bitmap != null)
            removeChild(bitmap);

        bitmap = new Bitmap(bmpDatas[clickStatus]);
        bitmap.x = -radius;
        bitmap.y = -radius;
        bitmap.width  = radius * 2;
        bitmap.height = radius * 2;

        addChild(bitmap);
    }

    function onEnterFrame(e : MouseEvent){
        this.rotation += 0.4 * rotationSpeed;
    }

    function onClicked(e : MouseEvent){
        clickStatus++;
        if(clickStatus >= bmpDatas.length){
            this.visible = false;
            Actuate.stop(this, null, false, false);
        }else{

            Actuate.stop(this, {scaleX : null, scaleY : null}, false, false);
            Actuate.tween(this, 0.25, {scaleX : this.scaleX * 0.8, scaleY : this.scaleY * 0.8})
                   .ease(Sine.easeIn)
                   .onComplete(
                        function(){
                            Actuate.tween(this, 1.0, {scaleX : 1.0, scaleY : 1.0})
                                   .ease(Bounce.easeOut);
                        });
        }

        clickStatus = Mathematics.min(bmpDatas.length - 1, clickStatus);
        reloadCookie();
    }
}
